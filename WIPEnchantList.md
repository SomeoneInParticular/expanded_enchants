# List of Enchantments

## Universal
* (WIP) **Reforming** (_I_)
  * :star: _Treasure Enchant_ :star:
  * When the item is sufficiently damaged, it will attempt to find the resources required to repair it in the player's inventory and automatically repair itself
  * Triggers at 2/3rds tool durability by default
  * One resource repairs 1/3rd of the tool's durability, with no XP cost
  * Incompatible with **Mending**

## Tools
* **Smelting** (_I_)
  * Automatically smelts the drops of a block broken by this tool
  * Interactions:
    * Is applied after **Fortune** (excess drops are calculated before smelting is attempted)
    * Is applied after **Silk Touch** (attempts to smelt the original block, rather than its drops)
    * (WIP) uses 1 XP per item smelted, taken from the XP produced by the breaking/smelting operation itself.
* **Pocketing** (_I_)
  * :star: _Treasure Enchant_ :star:
  * Drops from blocks destroyed by this tool are automatically placed within the player's inventory, if possible
  * Usable with all digging tools (WIP: Shears too)
  * Does not work with mob drops (see **Gathering**)
  * Interactions:
    * Drops are only pocketed if the tool used is appropriate for the block it's been used on
      * The hoe always counts as appropriate for crops, stems, and sugar cane
    * Applies after all enchants which effect drop contents (**Fortune**, **Silk Touch**, **Smelting** etc.)
    * Drops that do not fit in the Player's inventory are dropped as usual (at the broken block's position)
* (WIP) **Stretch** (_I-V_)
  * Increases the reach of the tool for the purposes of breaking blocks
  * 1 block of additional range added per level
  * Usable on all tools
  * Does not affect attack range on mobs (see **Reach**)
* (WIP) **Width** (I-III)
  * Extends the digging area by 1 per level in each direction *horizontally*
    * When digging down/up, this is considered to be to the left and right of the user
* (WIP) **Height** (I-III)
  * Extends the digging area by 1 per level in each direction *vertically*
    * When digging down/up, this is considered to be away from and towards the user****

## Armor
* (WIP) **Lukki Legs** (_I-III_)
  * Grow horrific legs which allow the player to "hover" in place when within 3/5/7 blocks of a solid surface (wall, ceiling, or floor)
  * Usable on any Chestplate _except_ the Elytra
  * Getting hit disables the ability until the player touches the ground (possibly with great force)
  * Entities wearing armor with this enchant are treated as Arthropods for the purpose of damage calculations (i.e. **Bane of Arthropods**)
* (WIP) **Desecrated** (_I-III_)
  * Corrupts the life within the user, twisting it into a necromantic form
  * Effects depend on level:
    * I: No effects (aside from Undead type change, see below)
    * II: Undead mobs (excluding the Wither) ignore you, unless threatened
    * III: Nearby undead mobs (excluding the Wither) will now attack entities which you have harmed or which have harmed you. Intelligent mobs will not risk themselves (such as by running into sunlight) to do so, however
  * Usable on any Chestplate _except_ the Elytra
  * Entities wearing armor with this enchant are treated as Undead for the purpose of damage calculations (i.e. **Smite**, hunger, and potion effects), but do not burn in sunlight

## Weapons
* (WIP) **Reach** (_I-V_)
  * Increases the reach of the weapon for the purposes of hitting enemies blocks
  * 0.5 block of additional range added per level
  * Usable on all melee weapons (swords, axes, and tridents)
  * Does not affect block interaction range (see **Stretch**)
* (WIP) **Gathering** (_I_)
  * :star: _Treasure Enchant_ :star:
  * Drops from mobs killed by this weapon are automatically placed within the player's inventory, if possible
  * Usable on all melee weapons
  * Does not work on block drops (see **Pocketing**)
  * Interactions:
    * Applies after all enchants which effect drops (**Fortune**, **Silk Touch**, **Smelting** etc.)