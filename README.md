# Expanded Enchants

Yet another enchantment mod for Minecraft, aimed to help expand the vanilla enchant system where it felt lacking before. All enchantments in this mod are designed to not feel out of place in a Vanilla gameplay experience, while not being too specialized or niche as to never be worth using (*cough* Bane of Arthropods *cough*).

The enchantments provided by this mod are built to be highly configurable, allowing players, server owners, and mod-pack creators to balance them to their liking (and even disable enchants that they feel are out of place or redundant).

Feel free to suggest new enchantments within issues (using the `suggestion` tag), though please keep in mind the following guidelines when doing so:
* Enchantments should not replace the original functionality of tool/armor
* Keep it simple to understand "at-a-glance" (the fewer conditions around it's use, the better)
* Keep in mind how the enchantment might interact with other enchantments, and specify if certain enchantment combinations should be disallowed as a result
* Try to make enchantments which expand upon existing mechanics in game, rather than adding new ones (Soul Speed and Frost-walker being the most extreme examples of this).
* Enchantments should be both fun to use and fun to play against!