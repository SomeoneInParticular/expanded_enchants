package someoneinpart.extended_enchants;

import net.fabricmc.api.ModInitializer;
import net.minecraft.text.TranslatableText;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import someoneinpart.extended_enchants.client.SoundManager;
import someoneinpart.extended_enchants.config.ConfigManager;
import someoneinpart.extended_enchants.enchantments.EnchantmentManager;

public final class ExtendedEnchants implements ModInitializer {
    public static final String MOD_ID = "extended_enchants";
    public static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void onInitialize() {
        // Rebuild the config directory if needed
        ConfigManager.prepareConfigDirectory();
        // Load configured values for our enchantments
        ConfigManager.initConfigs();

        // Load all sounds used in this mod
        SoundManager.init();

        // Initialize all enchantments in this mod
        EnchantmentManager.init();
    }

    /**
     * Helper function for generating translatable text with a well-established
     * lang key for configuration names
     * @param lead The leading element that should be used for the lang key
     * @param keys The remaining elements which should appear after the mod ID
     * @return A translatable text which uses a standardized lang key
     */
    public static TranslatableText buildLangKey(String lead, String ... keys) {
        String key = String.join(".", keys);
        return new TranslatableText(String.join(
                ".",lead, MOD_ID, key
        ));
    }
}
