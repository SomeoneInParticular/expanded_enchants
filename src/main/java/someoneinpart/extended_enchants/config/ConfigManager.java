package someoneinpart.extended_enchants.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.fabricmc.loader.api.FabricLoader;
import someoneinpart.extended_enchants.enchantments.EnchantmentManager;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;

import java.io.File;
import java.lang.reflect.Field;

import static someoneinpart.extended_enchants.ExtendedEnchants.LOGGER;

/**
 * A utility class which mediates all configs for this mod. Do not instantiate
 * an instance of this class!
 */
public final class ConfigManager {
    // The path that all config files for this mod are kept in
    public static final File CONFIG_PATH = new File(FabricLoader.getInstance().getConfigDir().toFile(), "extended_enchants");

    // JSON mediator to preserve my sanity, literally a copy-paste of ModMenu's
    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    /**
     * Establishes the config directory, creating it if it does not already exist
     */
    public static void prepareConfigDirectory() {
        if (CONFIG_PATH.mkdirs()) {
            LOGGER.warn("Rebuilt non-existent config directory");
        }
    }

    /**
     * Load configurable elements for all configs in the mod.
     */
    public static void initConfigs() {
        EnchantmentManager.POCKETING.getConfigMediator().load();
        EnchantmentManager.REFORMING.getConfigMediator().load();
        EnchantmentManager.SMELTING.getConfigMediator().load();
    }

    /**
     * Get the File reference for the config file by the key requested
     * @param key The key to fetch the config file for
     * @return The config file corresponding to the prior key
     */
    public static File getConfigFile(String key) {
        String child = key + ".json";
        return new File(CONFIG_PATH, child);
    }

    /**
     * Helper function to generate the full name of a config field in-code for
     * help in debugging
     * @param field The field to be referenced
     * @return A string representing the field's location in code
     */
    public static String getQualifiedConfigName(Field field) {
        return field.getDeclaringClass().getName() + "#" + field.getName();
    }

    /**
     * Converts a field key that uses camel-case define word separation into
     * separate words separated by an underscore '_'
     *
     * Derived from FieldNamingPolicy#LOWER_CASE_WITH_UNDERSCORES.translateName()
     */
    public static String getJsonKeyString(String key) {
        StringBuilder translation = new StringBuilder();
        for (int i = 0, length = key.length(); i < length; i++) {
            char character = key.charAt(i);
            if (Character.isUpperCase(character) && translation.length() != 0) {
                translation.append('_');
            }
            translation.append(character);
        }
        return translation.toString().toLowerCase();
    }

    public static void resetAllConfigs() {
        for (ConfigurableEnchantment enchantment : EnchantmentManager.CONFIGURABLE_ENCHANTS) {
            enchantment.getConfigMediator().resetAll();
        }
    }
}
