package someoneinpart.extended_enchants.config;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.text.MutableText;
import net.minecraft.util.Pair;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.config.options.ConfigOption;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Predicate;

import static someoneinpart.extended_enchants.ExtendedEnchants.LOGGER;

/**
 * Mediates between a config file and a given system's in-game settings.
 * Uses reflection to detect config options contained within itself, generating
 * JSON fields within the file and GUIs within the game to match
 */
public abstract class ConfigFileMediator {
    public final String KEY;
    protected File file;

    public ConfigFileMediator(String key) {
        this.KEY = key;
        this.file = ConfigManager.getConfigFile(key);
    }

    /**
     * Load the config from the file specified
     */
    public void load() {
        // If the file doesn't exist, rebuild it from the config's currently saved values and return
        if (!file.exists()) {
            LOGGER.warn("Config file '{}' did not exist; rebuilding with currently saved values.", file.getName());
            save();
            return;
        }

        // Attempt to set up a buffered reader for the target file
        try {
            // Prepare the workspace
            BufferedReader reader = new BufferedReader(new FileReader(file));
            JsonObject json;

            // Try and load the contents of the file
            try {
                json = JsonParser.parseReader(reader).getAsJsonObject();
            }
            catch (IllegalStateException e) {
                // If the file was malformed, rebuild it from scratch with currently stored values and return early
                LOGGER.warn("File '{}' was malformed, attempting to rebuild with current values", file.getName());
                if (!file.delete()) {
                    // If the file could not be rebuilt for some reason, crash out to prevent terrible bugs
                    String msg = String.format("Malformed config file '%s' could not be accessed or repaired.", file.getName());
                    throw new IllegalStateException(msg);
                }
                save();
                return;
            }

            // Get the elements of the JSON query in a map-like format
            HashMap<String, JsonElement> jsonEntries = new HashMap<>();
            json.entrySet().forEach(entry -> jsonEntries.put(entry.getKey(), entry.getValue()));

            // Try to update our fields using the contents of the JSON file
            for (Field field : this.collectConfigFields()) {
                // Try to get and unpack information on the desired field
                var fieldInfo = this.getConfigData(field);
                String key = fieldInfo.getLeft();
                var option = fieldInfo.getRight();

                // If the key returned null, continue onto the next element
                if (key == null) {
                    continue;
                }

                // If the option does not have a tooltip key specified, try to specify it
                if (option.getTooltipText() == null) {
                    String fieldName = ConfigManager.getJsonKeyString(field.getName());
                    MutableText tooltipText = ExtendedEnchants.buildLangKey(
                            "tooltip", this.getKey(), fieldName
                    );
                    option.setTooltipText(tooltipText);
                }

                // Try to get the value saved within the config file
                if (!jsonEntries.containsKey(key)) {
                    // If the config file did not have the key, report it and set the option to default
                    LOGGER.warn("Field '{}' did not have value in config file, falling back to default value '{}'",
                            field.getName(), option.getDefault());
                    option.reset();
                    continue;
                }
                JsonElement value = jsonEntries.get(key);
                jsonEntries.remove(key);

                // Try to update the option based on the JsonElement given
                LOGGER.debug("Option value, pre-update: {}", option.getValue());
                try {
                    option.readFromJson(value);
                } catch (Exception e) {
                    LOGGER.warn("Config '{}' in file '{}' was malformed, and was not used", key, file.getName());
                }
                LOGGER.debug("Option value, post-update: " + option.getValue());
            }

            // Report any elements in the config file which were not used
            for (String key : jsonEntries.keySet()) {
                LOGGER.warn("Key '{}' in file '{}' did not have a corresponding element in config class '{}', and was ignored.",
                        key, file.getName(), this.getClass().getName());
            }

        } catch (FileNotFoundException e) {
            LOGGER.error("Could not load config file '{}', skipping.", file.getName());
            e.printStackTrace();
        }

        LOGGER.debug("Finished loading file '{}'", file.getName());
    }

    /**
     * Save the (presumably updated) contents of this config
     */
    public void save() {
        // Initialize our empty JSON object, ready to parse to the file
        JsonObject json = new JsonObject();

        // Parse through the configurable fields and store their contents into JSON-serialized form
        List<Field> configOptions = collectConfigFields();
        for (Field field : configOptions) {
            // Parse and prepare the key for the JSON object
            var info = this.getConfigData(field);
            String key = info.getLeft();
            var option = info.getRight();

            // If the key returned null, skip to the next element
            if (key == null) {
                continue;
            }

            // Update the config file with the contents of the config option
            json.add(key, option.writeToJson());
        }

        // Save the newly formed JSON file
        String jsonString = ConfigManager.GSON.toJson(json);
        try (FileWriter writer = new FileWriter(this.file)) {
            writer.write(jsonString);
        } catch (IOException e) {
            LOGGER.error("Could not save configuration file '" + file.getName() + "'");
            e.printStackTrace();
        }

        LOGGER.debug("Finished saving file '" + file.getName() + "'");
    }

    public String getKey() {
        return KEY;
    }

    /**
     * Collects all configurable fields within this object for easy parsing
     */
    protected List<Field> collectConfigFields() {
        // Prepare the workspace
        Predicate<Field> configFieldFilter = field -> ConfigOption.class.isAssignableFrom(field.getType());
        Stack<Class<?>> classStack = new Stack<>();

        // Grab the list of all valid fields accessible to the class, including in superclasses
        List<Field> fields = new ArrayList<>();
        Class<?> currentClass = this.getClass();
        while (currentClass != ConfigFileMediator.class) {
            classStack.add(currentClass);
            currentClass = currentClass.getSuperclass();
        }
        while (!classStack.empty()) {
            Class<?> cls = classStack.pop();
            fields.addAll(Arrays.stream(cls.getDeclaredFields()).filter(configFieldFilter).toList());
        }

        return fields;
    }

    /**
     * Helper function to standardize fetching a ConfigOption object from this
     * class
     */
    protected ConfigOption<?> getConfigOption(Field field) {
        // Try to get the option bound to the field
        ConfigOption<?> option = null;
        String qualifiedName = ConfigManager.getQualifiedConfigName(field);
        try {
            option = (ConfigOption<?>) field.get(this);
        } catch (IllegalAccessException e) {
            LOGGER.error("Failed to access the contents of field '" + qualifiedName + "'");
            e.printStackTrace();
        }

        // Warn the user if the field's value was null somehow
        if (option == null) {
            LOGGER.warn("Field '" + qualifiedName + "' was null; did you initialize it?");
        }

        return option;
    }

    /**
     * Get data pertaining to a configurable option
     * @param field The field corresponding to an element that we want to configure
     * @return A 2-Tuple containing said field's identifying key (left) and the
     * ConfigurableOption instance which mediates its value in-code
     */
    protected Pair<String, ConfigOption<?>> getConfigData(Field field) {
        // Try to get the option bound to the field
        ConfigOption<?> option = getConfigOption(field);
        String qualifiedName = ConfigManager.getQualifiedConfigName(field);
        if (option == null) {
            LOGGER.warn("Field '{}' was null; have you initialized it?", qualifiedName);
            return null;
        }

        // Get/generate the config key for this option
        String key = option.getKey();

        // If the key is null, fall back to the name of the field instead
        if (key == null) {
            LOGGER.debug("Config key for field '{}' was null. Fell back to its name", qualifiedName);
            key = field.getName();
        }

        // Convert the key to our JSON-formatting
        key = ConfigManager.getJsonKeyString(key);

        return new Pair<>(key, option);
    }

    /**
     * Gets a list of data for all configurable element's managed by this object
     * @return List of 2-Tuples, each containing one field's identifying key
     * (left) and the ConfigurableOption instance which mediates its value
     * in-code (right)
     */
    public List<Pair<String, ConfigOption<?>>> getAllConfigData() {
        List<Field> fields = collectConfigFields();
        List<Pair<String, ConfigOption<?>>> infoList = new ArrayList<>();
        for (Field field : fields) {
            var info = getConfigData(field);
            if (info.getLeft() != null) {
                infoList.add(info);
            }
        }
        return infoList;
    }

    /**
     * Resets *all* config options managed by this manager to default
     */
    public void resetAll() {
        var optionData = getAllConfigData();
        for (var datum : optionData) {
            datum.getRight().reset();
        }
        this.save();
    }
}
