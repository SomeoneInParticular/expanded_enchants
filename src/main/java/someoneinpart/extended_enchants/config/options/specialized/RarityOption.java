package someoneinpart.extended_enchants.config.options.specialized;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.text.MutableText;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.client.widgets.config.specialized.RarityOptionWidget;
import someoneinpart.extended_enchants.config.options.generalized.EnumOption;

/**
 * Option which controls the rarity of the enchantment it is associated with.
 * Hard-bound to the Rarity enum in Minecraft, and always available to
 * configurable enchantments.
 */
public final class RarityOption extends EnumOption<Rarity> {

    RarityOption(Enum<Rarity> defaultValue) {
        super(defaultValue);
    }

    public static RarityOption build(Rarity defaultValue) {
        return new RarityOption(defaultValue);
    }

    @Override
    public ConfigBound buildConfigWidget() {
        return new RarityOptionWidget(this);
    }

    @Override
    public MutableText getLabelText() {
        // All rarity widgets should share the same label, to avoid needing 500 lang entries for the same thing
        return ExtendedEnchants.buildLangKey("widget", "config", "rarity", "label");
    }
}
