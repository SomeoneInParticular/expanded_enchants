package someoneinpart.extended_enchants.config.options.specialized;

import net.minecraft.text.MutableText;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.config.options.generalized.BooleanOption;

/**
 * Option which controls whether an enchantment should be enabled in game or not.
 * Defaults to true, and always marks itself as requiring a restart
 */
public final class EnabledOption extends BooleanOption {

    EnabledOption() {
        super(true);
    }

    public static EnabledOption build() {
        EnabledOption option = new EnabledOption();
        option.setRequiresRestart(true);
        return option;
    }

    public MutableText getLabelText() {
        // All enabled widgets should share the same label, to avoid needing 500 lang entries for the same thing
        return ExtendedEnchants.buildLangKey("widget", "config", "enabled", "label");
    }
}
