package someoneinpart.extended_enchants.config.options.generalized;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import someoneinpart.extended_enchants.client.widgets.config.generalized.BooleanOptionWidget;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.options.ConfigOption;

public class BooleanOption extends ConfigOption<Boolean> {

    protected BooleanOption(Boolean defaultValue) {
        super(defaultValue);
    }

    public static BooleanOption build(boolean defaultValue) {
        return new BooleanOption(defaultValue);
    }

    @Override
    public JsonElement writeToJson() {
        return new JsonPrimitive(this.value);
    }

    @Override
    public void readFromJson(JsonElement json) {
        this.value = json.getAsBoolean();
    }

    @Override
    public ConfigBound buildConfigWidget() {
        return new BooleanOptionWidget(this);
    }
}
