package someoneinpart.extended_enchants.config.options.generalized;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.client.widgets.config.generalized.EnumOptionWidget;
import someoneinpart.extended_enchants.config.options.ConfigOption;

public class EnumOption<T extends Enum<T>> extends ConfigOption<Enum<T>> {

    protected Class<T> enumClass;

    protected EnumOption(Enum<T> defaultValue) {
        super(defaultValue);
        this.enumClass = defaultValue.getDeclaringClass();
    }

    public static <T extends Enum<T>> EnumOption<T> build(T defaultValue) {
        return new EnumOption<>(defaultValue);
    }

    @Override
    public JsonElement writeToJson() {
        return new JsonPrimitive(this.value.toString());
    }

    @Override
    public void readFromJson(JsonElement json) {
        String jsonVal = json.getAsString();
        this.value = Enum.valueOf(enumClass, jsonVal);
    }

    @Override
    public ConfigBound buildConfigWidget() {
        return new EnumOptionWidget<>(this);
    }

    public Class<T> getEnumClass() {
        return this.enumClass;
    }
}
