package someoneinpart.extended_enchants.config.options.generalized;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.client.widgets.config.generalized.IntegerOptionWidget;
import someoneinpart.extended_enchants.config.options.ConfigOption;

public class IntegerOption extends ConfigOption<Integer> {
    // The maximum value that this option should be allowed to have
    protected int max;
    // The maximum value that this option should be allowed to have
    protected int min;

    protected IntegerOption(int defaultValue, int min, int max) {
        super(defaultValue);
        this.min = min;
        this.max = max;
    }

    public static IntegerOption build(int defaultValue, int min, int max) {
        return new IntegerOption(defaultValue, min, max);
    }

    @Override
    public JsonElement writeToJson() {
        return new JsonPrimitive(this.value);
    }

    @Override
    public void readFromJson(JsonElement json) {
        value = json.getAsInt();
        if (value > max) {
            value = max;
        } else if (value < min) {
            value = min;
        }
    }

    @Override
    public ConfigBound buildConfigWidget() {
        return new IntegerOptionWidget(this, this.min, this.max);
    }
}
