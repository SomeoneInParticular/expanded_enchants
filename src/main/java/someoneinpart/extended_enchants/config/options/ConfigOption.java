package someoneinpart.extended_enchants.config.options;

import com.google.gson.JsonElement;
import net.minecraft.text.MutableText;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.ConfigFileMediator;

import javax.annotation.Nullable;

public abstract class ConfigOption<T> {
    // The current value of the option
    protected T value;

    // Whether updating this option requires a game restart
    protected boolean requiresRestart = false;

    // The default option of the class
    protected final T defaultValue;

    // The text describing what this option does
    protected MutableText tooltipText = null;

    protected ConfigOption(T defaultValue) {
        this.defaultValue = defaultValue;
        this.value = defaultValue;
    }

    // Get the contents of this element
    public T getValue() {
        return this.value;
    }

    // Set the value of the element explicitly
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Save the value stored in this option to a JsonElement
     */
    public abstract JsonElement writeToJson();

    /**
     * Load the value from a passed JsonElement into this object
     */
    public abstract void readFromJson(JsonElement json);

    // Get the default value of the element
    public T getDefault() {
        return defaultValue;
    }

    // Reset the value to its default
    public void reset() {
        value = defaultValue;
    }

    /**
     * Default implementation which returns null, delegating key determination
     * to the ConfigManager.
     *
     * See {@link ConfigFileMediator}
     */
    public String getKey() {
        return null;
    }

    // Return the widget that Config GUIs should use for this option
    public abstract ConfigBound buildConfigWidget();

    /**
     * Return an explicit label to use in config options. A null return will
     * have the GUI autogenerate the label based on the context of the option
     */
    @Nullable
    public MutableText getLabelText() {
        return null;
    }

    /**
     * Reports if changes to this config value require a restart to take effect
     */
    public boolean requiresRestart() {
        return this.requiresRestart;
    }

    /**
     * Specify whether a widget requires a restart to take effect or not
     */
    public void setRequiresRestart(boolean val) {
        this.requiresRestart = val;
    }

    /**
     * Get the tooltip text for this element
     */
    public MutableText getTooltipText() {
        return this.tooltipText;
    }

    /**
     * Explicitly set the tooltip text for this option
     */
    public ConfigOption<T> setTooltipText(MutableText tooltipText) {
        this.tooltipText = tooltipText;
        return this;
    }
}
