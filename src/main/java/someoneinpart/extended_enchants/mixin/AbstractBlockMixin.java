package someoneinpart.extended_enchants.mixin;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import java.util.List;

import static someoneinpart.extended_enchants.enchantments.EnchantmentManager.POCKETING;
import static someoneinpart.extended_enchants.enchantments.EnchantmentManager.SMELTING;

@Mixin(value = AbstractBlock.class, priority = 10000)
abstract class AbstractBlockMixin {

    /**
     * Inject which allows enchantments in this mod to modify a block's drops,
     * after any drop-modifying processes have been completed
     */
    @Inject(
            at = @At(value = "RETURN", ordinal = 1),
            method = "getDroppedStacks",
            cancellable = true,
            locals = LocalCapture.CAPTURE_FAILSOFT
    )
    private void updateDroppedStacks(BlockState state, LootContext.Builder builder, CallbackInfoReturnable<List<ItemStack>> cir, Identifier identifier, LootContext lootContext) {
        // Get the values required to check and/or pass to our enchantments
        ItemStack toolStack = lootContext.get(LootContextParameters.TOOL);
        // Return early if the tool is somehow null
        if (toolStack == null) return;

        // Otherwise, update the drops based on the set of enchantments
        List<ItemStack> drops = cir.getReturnValue();
        // Smelting enchant (transform smelt-able items into their smelted counterparts)
        if (EnchantmentHelper.getLevel(SMELTING, toolStack) > 0) {
            drops = SMELTING.autoSmeltDrops(drops, (AbstractBlock) (Object) this, lootContext);
        }
        // Pocketing enchant (deposit as many drops as possible into the player's inventory)
        if (EnchantmentHelper.getLevel(POCKETING, toolStack) > 0) {
            // Duck typing required to avoid errors, hence the double cast
            POCKETING.pocketDrops(drops, (AbstractBlock) (Object) this, lootContext);
        }
        // Update the drops with the final resulting set of ItemStacks
        cir.setReturnValue(drops);
    }
}
