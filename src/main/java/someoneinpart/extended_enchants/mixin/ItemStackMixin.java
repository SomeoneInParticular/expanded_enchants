package someoneinpart.extended_enchants.mixin;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import java.util.Random;

import static someoneinpart.extended_enchants.enchantments.EnchantmentManager.REFORMING;

@Mixin(value = ItemStack.class)
public abstract class ItemStackMixin {
    @ModifyVariable(
            method = "damage(ILjava/util/Random;Lnet/minecraft/server/network/ServerPlayerEntity;)Z",
            at = @At(value = "STORE", ordinal = 1),
            ordinal = 1
    )
    private int reformRepair(int i, int amount, Random random, ServerPlayerEntity player) {
        ItemStack castStack = (ItemStack) (Object) this;
        int level = EnchantmentHelper.getLevel(REFORMING, castStack);
        if (level > 0) {
            i = REFORMING.attemptDamageRepair(
                    castStack, i, level, player
            );
        }
        return i;
    }
}
