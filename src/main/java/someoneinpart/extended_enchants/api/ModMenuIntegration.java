package someoneinpart.extended_enchants.api;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import someoneinpart.extended_enchants.client.screens.ConfigLandingScreen;

public class ModMenuIntegration implements ModMenuApi {

    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return ConfigLandingScreen::new;
    }
}
