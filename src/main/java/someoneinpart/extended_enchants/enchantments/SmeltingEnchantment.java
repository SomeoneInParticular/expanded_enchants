package someoneinpart.extended_enchants.enchantments;

import net.minecraft.block.AbstractBlock;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.world.World;
import someoneinpart.extended_enchants.config.options.generalized.BooleanOption;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;
import someoneinpart.extended_enchants.enchantments.templates.EnchantmentConfigMediator;

import java.util.ArrayList;
import java.util.List;

/**
 * An enchantment which automatically smelts block drops (based on furnace
 * recipes). Applies after most enchantments which modify block drops (i.e.
 * Fortune and Silk Touch), but before Pocketing.
 */
public class SmeltingEnchantment extends ConfigurableEnchantment {

    // Tracks and manages the configuration of this enchantment
    protected final SmeltingConfigMediator configManager;

    public SmeltingEnchantment(EquipmentSlot ... slotTypes) {
        super(EnchantmentTarget.DIGGER, slotTypes);
        configManager = new SmeltingConfigMediator();
    }

    @Override
    public EnchantmentConfigMediator getConfigMediator() {
        return configManager;
    }

    public List<ItemStack> autoSmeltDrops(List<ItemStack> drops, AbstractBlock block, LootContext lootContext) {
        // If the block was broken by the ether somehow, pass over any drop modifications
        ItemStack toolStack = lootContext.get(LootContextParameters.TOOL);
        if (toolStack == null) return drops;

        // Check if the correct tool is being used, and if it should smelt blocks regardless
        if (configManager.requiresCorrectTool.getValue()) {
            if (!EnchantmentManager.isEffectiveOn(toolStack, block)) {
                return drops;
            }
        }

        // Smelt all applicable drops if the prior checks pass
        List<ItemStack> newDrops = new ArrayList<>(drops.size());
        for (ItemStack drop : drops) {
            SimpleInventory proxyInv = new SimpleInventory(drop);
            World world = lootContext.getWorld();
            Recipe<Inventory> recipe = world.getRecipeManager().getFirstMatch(RecipeType.SMELTING, proxyInv, world).orElse(null);
            // If no recipe is found, just pass through the original drop
            if (recipe == null) {
                newDrops.add(drop);
                // Otherwise, replace the drop with a set of (smelted) items
            } else {
                // Parse the recipe's contents for later use
                ItemStack smeltProduct = recipe.getOutput();
                Item smeltItem = smeltProduct.getItem();
                int productCount = smeltProduct.getCount();
                // Calculate the number of items produced from smelting this stack
                int initialCount = drop.getCount();
                int remainingCount = initialCount * productCount;
                int maxItemCount = smeltProduct.getItem().getMaxCount();
                // Break the result into multiple stacks if needed
                while (remainingCount > maxItemCount) {
                    remainingCount -= maxItemCount;
                    newDrops.add(new ItemStack(smeltItem, maxItemCount));
                }
                newDrops.add(new ItemStack(smeltItem, remainingCount));
            }
        }
        return newDrops;
    }

    public static class SmeltingConfigMediator extends EnchantmentConfigMediator {

        public SmeltingConfigMediator() {
            super("smelting", Rarity.RARE);
        }
        // Whether this will only function if the tool being used is correct
        public final BooleanOption requiresCorrectTool = BooleanOption.build(false);
    }
}
