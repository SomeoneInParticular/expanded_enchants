package someoneinpart.extended_enchants.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.network.ServerPlayerEntity;
import someoneinpart.extended_enchants.config.options.generalized.BooleanOption;
import someoneinpart.extended_enchants.config.options.generalized.IntegerOption;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;
import someoneinpart.extended_enchants.enchantments.templates.EnchantmentConfigMediator;

import static someoneinpart.extended_enchants.client.SoundManager.REFORM;

/**
 * An alternative to mending which automatically repairs the tool using resources
 * in the player's inventory. At default, this costs no experience and provides
 * more durability per resource than an Anvil would (to keep it competitive with
 * Mending). It can also use Prismarine Crystals as "universal" material by
 * default (namely to avoid Netherite tools being utterly ludicrous to repair),
 * Unlike Mending, this can also be used to repair bows with Infinity,
 * effectively converting wood into arrows for the user.
 */
public class ReformingEnchantment extends ConfigurableEnchantment {

    ReformingConfigMediator configManager = new ReformingConfigMediator();

    public ReformingEnchantment(EquipmentSlot... slots) {
        super(EnchantmentTarget.BREAKABLE, slots);
    }

    @Override
    public EnchantmentConfigMediator getConfigMediator() {
        return configManager;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        // This enchantment is incompatible with mending
        if (other == Enchantments.MENDING) {
            return false;
        }
        return super.canAccept(other);
    }

    @Override
    public int getMaxLevel() {
        // TODO: Make this configurable
        return 3;
    }

    /**
     * Attempt to repair damage on the target item <br>
     *
     * The repair ration is calculated by the following equation: <br>
     * (l * s + b) / (l * s + b + o) <br>
     * where 'o' is the initial offset
     * @param repairStack The item stack to attempt to repair
     * @param initialDamage The damage the item was about to take
     * @param enchantLevel The level of Reforming on the item in question
     * @return The new damage the item should be subject too
     */
    public int attemptDamageRepair(ItemStack repairStack, int initialDamage, int enchantLevel, ServerPlayerEntity player) {
        // Calculate the amount of durability repaired by a stack of the correct material
        int resourceRepairAmount = calculateMaterialRepairAmount(enchantLevel, repairStack.getMaxDamage());
        int materialRepairedDamage = initialDamage - resourceRepairAmount;

        // Calculate the damage this item would have after a repair by omni-resource
        int omniRepairAmount = calculateOmniRepairAmount(enchantLevel);
        int omniRepairedDamage = initialDamage - omniRepairAmount;

        // Whether we should attempt a repair, and with the repair or omni-resource
        boolean shouldMaterialRepair;
        boolean shouldOmniRepair;
        if (configManager.onlyReformOnBreak.getValue()) {
            shouldMaterialRepair = initialDamage >= repairStack.getMaxDamage();
            shouldOmniRepair = initialDamage >= repairStack.getMaxDamage();
        } else {
            shouldMaterialRepair = (materialRepairedDamage >= 0 || initialDamage >= repairStack.getMaxDamage());
            shouldOmniRepair = (omniRepairedDamage >= 0 || initialDamage >= repairStack.getMaxDamage());
        }

        // If either of the checks above passed, attempt to repair the tool
        if ((shouldMaterialRepair || shouldOmniRepair) && player != null) {
            ItemStack resourceStack = null;
            int stackIdx = -1;
            int newDamage = -1;
            PlayerInventory inventory = player.getInventory();
            for (int idx = 0; idx < inventory.size(); idx++) {
                ItemStack stack = inventory.getStack(idx);
                // If we can perform an omni repair and found the resource required for it, finish
                if (shouldOmniRepair && stack.getItem() == Items.PRISMARINE_CRYSTALS) {
                    resourceStack = stack;
                    stackIdx = idx;
                    newDamage = omniRepairedDamage;
                    break;
                } else if (shouldMaterialRepair && repairStack.getItem().canRepair(repairStack, stack)) {
                    resourceStack = stack;
                    stackIdx = idx;
                    newDamage = materialRepairedDamage;
                    break;
                }
            }
            // If a valid stack was found, reduce its contents by one and repair the tool
            if (resourceStack != null) {
                resourceStack.decrement(1);
                if (resourceStack.isEmpty()) {
                    inventory.setStack(stackIdx, ItemStack.EMPTY);
                }
                // Play a sound to indicate that the repair worked
                float pitch = player.world.getRandom().nextFloat() * 0.4f + 0.8f;
                player.world.playSound(null, player.getX(), player.getY(), player.getZ(), REFORM, player.getSoundCategory(), 0.3f, pitch);
                return newDamage;
            }
        }

        // If all checks above fail, return the damage untouched
        return initialDamage;
    }

    protected int calculateMaterialRepairAmount(int enchantLevel, int maxDurability) {
        int c = configManager.resourceRepairBase.getValue();
        int m = configManager.resourceRepairGain.getValue();
        double ratio = 1.0 / (c + (enchantLevel-1)*m);
        return (int) (ratio * maxDurability);
    }

    protected int calculateOmniRepairAmount(int enchantLevel) {
        int c = configManager.omniRepairBase.getValue();
        int m = configManager.omniRepairGain.getValue();
        return c + ((enchantLevel-1)*m);
    }

    public static class ReformingConfigMediator extends EnchantmentConfigMediator {
        protected ReformingConfigMediator() {
            super("reforming", Rarity.UNCOMMON);
        }

        // The amount of resources to repair an item from fully damaged at enchant level 1
        public final IntegerOption resourceRepairBase = IntegerOption.build(4, 1,  20);

        // The reduction in resource cost for a full repair per Reforming level above 1
        public final IntegerOption resourceRepairGain = IntegerOption.build(1, 1,  10);

        // How much durability one Omni-resource provides at Reforming level 1
        public final IntegerOption omniRepairBase = IntegerOption.build(200, 1,  5000);

        // How much durability additional levels of Reforming grant when using Omni-resources
        public final IntegerOption omniRepairGain = IntegerOption.build(100, 1,  2000);

        // Whether the tool should only attempt to repair itself when it would break
        public final BooleanOption onlyReformOnBreak = BooleanOption.build(true);
    }
}
