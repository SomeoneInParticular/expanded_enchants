package someoneinpart.extended_enchants.enchantments;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;
import someoneinpart.extended_enchants.mixin.MiningToolItemMixin;

import java.util.ArrayList;

/**
 * Manages registration and access to enchantments added within this mod
 */
public final class EnchantmentManager {

    // Tool enchants
    public static final SmeltingEnchantment SMELTING = new SmeltingEnchantment(EquipmentSlot.MAINHAND);
    public static final PocketingEnchantment POCKETING = new PocketingEnchantment(EquipmentSlot.MAINHAND);
    public static final ReformingEnchantment REFORMING = new ReformingEnchantment(EquipmentSlot.values());

    // List of known configurable enchantments for easy GUI generation
    public static final ArrayList<ConfigurableEnchantment> CONFIGURABLE_ENCHANTS = new ArrayList<>();

    /**
     * Copy of {@link Enchantment} that automatically appends the required
     * localization tags for this mod
     * @param name The name of the enchantment (excluding its localization key)
     * @param enchantment The enchantment to register
     */
    private static void registerEnchant(String name, Enchantment enchantment) {
        Identifier enchantID = new Identifier(ExtendedEnchants.MOD_ID, name);
        ExtendedEnchants.LOGGER.info("Registering Enchantment " + enchantID);

        if (enchantment instanceof ConfigurableEnchantment) {
            CONFIGURABLE_ENCHANTS.add((ConfigurableEnchantment) enchantment);
            // Only register the enchantment if it has been enabled
            if (((ConfigurableEnchantment) enchantment).getConfigMediator().enabled.getValue()) {
                Registry.register(Registry.ENCHANTMENT, enchantID, enchantment);
            }
        } else {
            Registry.register(Registry.ENCHANTMENT, enchantID, enchantment);
        }
    }

    public static void init() {
        registerEnchant("smelting", SMELTING);
        registerEnchant("pocketing", POCKETING);
        registerEnchant("reforming", REFORMING);
    }

    /**
     * Helper function which checks whether the tool supplied is effective on
     * the block provided
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isEffectiveOn(ItemStack toolStack, AbstractBlock block) {
        if (!(toolStack.getItem() instanceof MiningToolItem)) return false;
        Tag<Block> effectiveBlocks = ((MiningToolItemMixin) toolStack.getItem()).getEffectiveBlocksTag();
        return effectiveBlocks.contains((Block) block);
    }
}
