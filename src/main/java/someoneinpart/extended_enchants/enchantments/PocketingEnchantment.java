package someoneinpart.extended_enchants.enchantments;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.CropBlock;
import net.minecraft.block.StemBlock;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import someoneinpart.extended_enchants.config.options.generalized.BooleanOption;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;
import someoneinpart.extended_enchants.enchantments.templates.EnchantmentConfigMediator;

import java.util.Collections;
import java.util.List;

/**
 * An enchantment which automatically deposits drops taken from a block into
 * the player's inventory, if possible. Only captures items from natural block
 * drops (not chests or other inventories) and if there is room in the player's
 * inventory to deposit the drops into.
 */
public class PocketingEnchantment extends ConfigurableEnchantment {
    // Tracks and manages the configuration of this enchantment
    protected final PocketingConfigMediator configManager;

    public PocketingEnchantment(EquipmentSlot ... slotTypes) {
        super(EnchantmentTarget.DIGGER, slotTypes);
        this.configManager = new PocketingConfigMediator();
    }

    public void pocketDrops(List<ItemStack> drops, AbstractBlock block, LootContext lootContext) {
        // Get the entity that broke the block with the tool
        Entity entity = lootContext.get(LootContextParameters.THIS_ENTITY);
        // Return early if the entity is not valid
        if (entity == null || !entity.isPlayer() || entity.isSpectator()) {
            return;
        }

        // If the block was broken by the ether somehow, pass over any drop modifications
        ItemStack toolStack = lootContext.get(LootContextParameters.TOOL);
        if (toolStack == null) return;

        // Special case; crops are treated as always compatible unless the crops_require_hoe config is enables
        if (block instanceof CropBlock || block instanceof StemBlock) {
            if (configManager.cropsRequireHoe.getValue() && !(toolStack.getItem() instanceof HoeItem)) {
                return;
            }
        }
        // Check whether the tool should work on blocks it is not effective on
        else if (configManager.requiresCorrectTool.getValue()) {
            if (!EnchantmentManager.isEffectiveOn(toolStack, block)) return;
        }

        // Get access to the player's inventory
        PlayerEntity player = (PlayerEntity) entity;
        PlayerInventory inventory = player.getInventory();

        // Insert the (potentially shuffled) drops one stack at a time
        if (configManager.shouldShuffleDrops.getValue()) {
            Collections.shuffle(drops);
        }
        for (ItemStack drop : drops) {
            // Note; this updates the contents of 'drop' itself, so we don't need to update our drops list explicitly
            inventory.insertStack(drop);
        }
    }

    @Override
    public EnchantmentConfigMediator getConfigMediator() {
        return configManager;
    }

    public static class PocketingConfigMediator extends EnchantmentConfigMediator {
        // This config is bound to the 'pocketing.json' file
        protected PocketingConfigMediator() {
            super("pocketing", Rarity.RARE);
        }

        // Whether this will only function if the tool being used is correct
        public final BooleanOption requiresCorrectTool = BooleanOption.build(false);
        // Whether only hoes will collect crop drops
        public final BooleanOption cropsRequireHoe = BooleanOption.build(false);
        // Whether drops should be shuffled before determining whether they can be placed in the player's inventory or not
        public final BooleanOption shouldShuffleDrops = BooleanOption.build(true);
    }
}
