package someoneinpart.extended_enchants.enchantments.templates;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;

/**
 * Defines and enchantment which has configurable values, as defined by a
 * ConfigMediator instance.
 */
public abstract class ConfigurableEnchantment extends Enchantment {

    protected ConfigurableEnchantment(EnchantmentTarget type, EquipmentSlot[] slotTypes) {
        // Weight is mediated via the enchantment mediator, rather than hard-coded
        super(null, type, slotTypes);
    }

    @Override
    public Rarity getRarity() {
        return (Rarity) this.getConfigMediator().rarity.getValue();
    }

    /**
     * Get the config mediator which manages configurable parameters for this
     * enchantment.
     */
    public abstract EnchantmentConfigMediator getConfigMediator();
}
