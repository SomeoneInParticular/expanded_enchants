package someoneinpart.extended_enchants.enchantments.templates;

import net.minecraft.enchantment.Enchantment;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.config.ConfigFileMediator;
import someoneinpart.extended_enchants.config.options.specialized.EnabledOption;
import someoneinpart.extended_enchants.config.options.specialized.RarityOption;

/**
 * An extension of the default ConfigMediator which adds some universal config
 * options for Enchantments, namely the ability to be enabled/disabled, and
 * how rare they should be.
 */
public abstract class EnchantmentConfigMediator extends ConfigFileMediator {
    // Whether the enchantment should be enabled in game or not
    public final EnabledOption enabled;
    // The rarity the enchantment should have
    public final RarityOption rarity;

    public EnchantmentConfigMediator(String key, Enchantment.Rarity rarity) {
        super(key);
        // Whether the enchantment is enabled or not requires a restart to take effect
        this.enabled = (EnabledOption) EnabledOption.build().setTooltipText(
                ExtendedEnchants.buildLangKey("tooltip", "enabled")
        );
        // Rarity, however, does not (despite needing to be provided during registration; a null value works fine)
        this.rarity = (RarityOption) RarityOption.build(rarity).setTooltipText(
                ExtendedEnchants.buildLangKey("tooltip", "rarity")
        );
    }
}
