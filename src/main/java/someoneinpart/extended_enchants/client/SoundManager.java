package someoneinpart.extended_enchants.client;

import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import someoneinpart.extended_enchants.ExtendedEnchants;

public final class SoundManager {
    public static SoundEvent REFORM;

    static SoundEvent registerSoundEvent(String name) {
        Identifier id = new Identifier(ExtendedEnchants.MOD_ID, name);
        SoundEvent fullEvent = new SoundEvent(id);
        Registry.register(Registry.SOUND_EVENT, id, fullEvent);
        return fullEvent;
    }

    public static void init() {
        REFORM = registerSoundEvent("reform");
    }
}
