package someoneinpart.extended_enchants.client.gui;

import io.github.cottonmc.cotton.gui.client.BackgroundPainter;
import io.github.cottonmc.cotton.gui.client.LightweightGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WPanel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WScrollPanel;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.github.cottonmc.cotton.gui.widget.data.Insets;
import net.fabricmc.fabric.api.util.TriState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.widgets.VerticallyCenteredElementsPanelWidget;

import javax.annotation.Nullable;

/**
 * A GUI description built to mimic the appearance and functionality of the
 * vanilla settings (specifically the KeyBind menu), but with LibGUIs dark mode
 * support and the ability to accept any panel of elements to scroll through
 */
public class VanillaSettingsMimicGUI extends LightweightGuiDescription {
    // The default size of the buttons along the bottom
    public static final int DEFAULT_BUTTON_WIDTH = 150;
    public static final int DEFAULT_BUTTON_HEIGHT = 20;

    // The default space between buttons in the button panel
    public static final int DEFAULT_BUTTON_SPACING = 10;

    public VanillaSettingsMimicGUI(Screen previous, WPanel contentsPanel, @Nullable WButton button) {
        // This gui should be treated as fullscreen, with a centered title
        this.setFullscreen(true);
        this.setTitleAlignment(HorizontalAlignment.CENTER);

        // Generate the actual scrolling panel
        WScrollPanel scrollPanel = new WScrollPanel(contentsPanel) {
            @Override
            public void setSize(int x, int y) {
                super.setSize(x, y);
                // Resize the scroll panel to the correct proportions
                contentsPanel.setSize(x, contentsPanel.getY());
                // Recalculate the layout
                this.layout();
            }
        };
        scrollPanel.setBackgroundPainter(BackgroundPainter.createColorful(0x77000000));
        scrollPanel.setScrollingVertically(TriState.TRUE);
        scrollPanel.setScrollingHorizontally(TriState.FALSE);

        // The 'Done' button, which returns you to the prior window
        WButton doneButton = new WButton();
        doneButton.setLabel(ExtendedEnchants.buildLangKey("widget", "label", "done"));
        doneButton.setOnClick(() -> MinecraftClient.getInstance().setScreen(previous));

        // A panel which holds the 'Done' and 'Other' buttons
        WPlainPanel buttonPanel = new WPlainPanel();
        buttonPanel.add(button, 0, 0, DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT);
        if (button != null) {
            buttonPanel.add(doneButton, button.getWidth()+DEFAULT_BUTTON_SPACING, 0, DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT);
        }

        // A centering panel to keep the prior panel centered at all times
        VerticallyCenteredElementsPanelWidget buttonCenteringPanel = new VerticallyCenteredElementsPanelWidget();
        buttonCenteringPanel.addCentered(buttonPanel, 0);

        // Build the (fullscreen) root panel
        WPlainPanel root = new WPlainPanel() {
            @Override
            public void setSize(int x, int y) {
                super.setSize(x, y);
                // Resize the scroll panel to the correct proportions
                Insets insets = this.getInsets();
                scrollPanel.setSize(
                        x-(insets.left()+insets.right()),
                        y-(insets.top()+insets.bottom())
                );
                // Resize AND reposition the button panel
                buttonCenteringPanel.setSize(
                        x-(insets.left()+insets.right()),
                        insets.bottom()
                );
                buttonCenteringPanel.setLocation(
                        0,
                        this.height-((insets.bottom()+DEFAULT_BUTTON_HEIGHT)/2)
                );
            }
        };
        root.setBackgroundPainter(null);
        root.setInsets(new Insets(20, 0, 30, 0));

        // Add the top-level elements to our root panel
        root.add(scrollPanel, 0, 0);
        root.add(buttonCenteringPanel, 0, 20);

        // Finalize everything
        this.setRootPanel(root);
        root.validate(this);
    }
}
