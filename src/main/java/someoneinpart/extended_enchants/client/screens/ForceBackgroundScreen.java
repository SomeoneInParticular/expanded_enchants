package someoneinpart.extended_enchants.client.screens;

import io.github.cottonmc.cotton.gui.GuiDescription;
import io.github.cottonmc.cotton.gui.client.CottonClientScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;

public abstract class ForceBackgroundScreen extends CottonClientScreen {
    public ForceBackgroundScreen(Text title, GuiDescription description) {
        super(title, description);
    }

    public ForceBackgroundScreen(GuiDescription description) {
        super(description);
    }

    // Override which forces the background texture to always be rendered
    @Override
    public void renderBackground(MatrixStack matrices, int vOffset) {
        this.renderBackgroundTexture(vOffset);
    }
}
