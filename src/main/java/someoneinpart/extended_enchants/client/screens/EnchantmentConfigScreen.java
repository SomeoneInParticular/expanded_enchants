package someoneinpart.extended_enchants.client.screens;

import io.github.cottonmc.cotton.gui.GuiDescription;
import io.github.cottonmc.cotton.gui.widget.TooltipBuilder;
import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.util.Formatting;
import net.minecraft.util.Pair;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.gui.VanillaSettingsMimicGUI;
import someoneinpart.extended_enchants.client.widgets.VanillaSettingsEntryMimicWidget;
import someoneinpart.extended_enchants.client.widgets.VerticallyCenteredElementsPanelWidget;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.options.ConfigOption;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;
import someoneinpart.extended_enchants.enchantments.templates.EnchantmentConfigMediator;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * A generic config screen for a single enchantment's configuration options.
 * Automatically builds its contents based on the ConfigMediator for the
 * given enchantment.
 */
@Environment(value=EnvType.CLIENT)
public class EnchantmentConfigScreen extends ForceBackgroundScreen {

    // The tooltip text that a widget should display if it requires a restart
    static final MutableText REQUIRES_RESTART_TEXT = ExtendedEnchants.buildLangKey("tooltip", "requires_restart")
            .setStyle(Style.EMPTY.withColor(Formatting.RED));

    // The prior screen which opened this one, to open again when it closes
    Screen prior;

    // The config manager to load/save configs when the screen open/closes
    EnchantmentConfigMediator manager;

    /**
     * Should not be used directly, use {@link EnchantmentConfigScreen#build} instead
     */
    protected EnchantmentConfigScreen(GuiDescription gui, EnchantmentConfigMediator manager, Screen prior) {
        super(gui);

        // Bind everything for easy management
        this.prior = prior;
        this.manager = manager;
        this.manager.load();
    }

    /**
     * Constructor-by-proxxy function for EnchantmentConfigScreen.
     * Used to avoid reduce the number of (potentially very expensive) reflective
     * calls overall, and required to side-step the "super call must be first
     * statement in constructor" error in constructors
     */
    public static EnchantmentConfigScreen build(ConfigurableEnchantment enchantment, Screen prior) {
        // Initialize the panel that will hold the actual contents of this screen
        VerticallyCenteredElementsPanelWidget contentPanel = new VerticallyCenteredElementsPanelWidget();

        // Initialize a list of widgets that will need to be updated when a full reset is called
        ArrayList<ConfigBound> boundWidgets = new ArrayList<>();

        // Grab the config manager for this enchantment for easy access
        EnchantmentConfigMediator manager = enchantment.getConfigMediator();

        // Run through the list of all config options required to be accounted for
        ListIterator<Pair<String, ConfigOption<?>>> iter = manager.getAllConfigData().listIterator();
        while (iter.hasNext()) {
            // Track the current index and the data in said index
            int currentIdx = iter.nextIndex();
            Pair<String, ConfigOption<?>> value = iter.next();

            // Unpack the datum for better readability
            String id = value.getLeft();
            ConfigOption<?> option = value.getRight();

            // First, generate the widget to be associated with this option
            ConfigBound boundWidget = option.buildConfigWidget();

            // Track this widget for later
            boundWidgets.add(boundWidget);

            // Build up the panel that our content panel will use internally
            MutableText langText = option.getLabelText();
            if (langText == null) {
                langText = ExtendedEnchants.buildLangKey("widget", "config", manager.getKey(), id, "label");
            }
            WWidget newEntry = generateEntry(langText, option, boundWidget);

            // Add said panel to our content panel for management
            int verticalOffset = currentIdx * VanillaSettingsEntryMimicWidget.DEFAULT_HEIGHT;
            contentPanel.addCentered(newEntry, verticalOffset);
        }

        // Build the 'Reset All' button, based on the set of config options collected prior
        WButton resetButton = new WButton();
        resetButton.setLabel(ExtendedEnchants.buildLangKey("widget", "label", "reset_all"));
        resetButton.setOnClick(() -> {
            manager.resetAll();
            boundWidgets.forEach(ConfigBound::onChange);
        });

        // Finally, build the actual GUI description to be used in this screen
        VanillaSettingsMimicGUI gui = new VanillaSettingsMimicGUI(prior, contentPanel, resetButton);

        // And use it to build our screen
        return new EnchantmentConfigScreen(gui, manager, prior);
    }

    @Override
    public void removed() {
        super.removed();
        this.manager.save();
    }

    protected static VanillaSettingsEntryMimicWidget generateEntry(MutableText text, ConfigOption<?> option, ConfigBound boundWidget) {
        // A reset button, which sets the value back to its default
        text = text.setStyle(Style.EMPTY.withColor(0xFFFFFF));
        WButton resetButton = new WButton(ExtendedEnchants.buildLangKey("widget", "label", "reset"));
        resetButton.setOnClick(() -> {
            option.reset();
            boundWidget.onChange();
        });

        // Generate the entry
        return new VanillaSettingsEntryMimicWidget(text, (WWidget) boundWidget, resetButton) {
            @Override
            public void addTooltip(TooltipBuilder tooltipBuilder) {
                // Add the tooltip text the option has bound to it, if any
                if (option.getTooltipText() != null) {
                    MinecraftClient.getInstance().textRenderer.wrapLines(option.getTooltipText(), 200).forEach(tooltipBuilder::add);
                }
                if (option.requiresRestart()) {
                    tooltipBuilder.add(REQUIRES_RESTART_TEXT);
                }
            }
        };
    }
}
