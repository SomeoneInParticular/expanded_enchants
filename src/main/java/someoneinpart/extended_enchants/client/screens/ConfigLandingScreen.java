package someoneinpart.extended_enchants.client.screens;

import io.github.cottonmc.cotton.gui.widget.WButton;
import io.github.cottonmc.cotton.gui.widget.WPanel;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.gui.VanillaSettingsMimicGUI;
import someoneinpart.extended_enchants.client.widgets.VanillaSettingsEntryMimicWidget;
import someoneinpart.extended_enchants.client.widgets.VerticallyCenteredElementsPanelWidget;
import someoneinpart.extended_enchants.config.ConfigManager;
import someoneinpart.extended_enchants.enchantments.EnchantmentManager;
import someoneinpart.extended_enchants.enchantments.templates.ConfigurableEnchantment;

import java.util.ListIterator;

/**
 * The screen that is opened when the settings button is initially clicked in
 * ModMenu. Lists out all configurable enchantments in this mod, allowing
 * for each of them to be configured independently of one another
 */
@Environment(value=EnvType.CLIENT)
public class ConfigLandingScreen extends ForceBackgroundScreen {

    Screen prior;

    public ConfigLandingScreen(Screen prior) {
        super(
                ExtendedEnchants.buildLangKey("gui", "config_navigation", "title"),
                new VanillaSettingsMimicGUI(prior, generateContentsPanel(), generateResetAllButton())
        );
        this.prior = prior;
    }

    @Override
    public void onClose() {
        if (this.client != null) {
            this.client.setScreen(prior);
        } else {
            super.onClose();
        }
    }

    /**
     * Generate the panel that contains all the config options for this screen
     */
    protected static WPanel generateContentsPanel() {
        // Initialize the panel which will actually contain everything
        VerticallyCenteredElementsPanelWidget contentsPanel = new VerticallyCenteredElementsPanelWidget();

        // Fill the panel with our navigation widgets, one per enchant (sorted alphabetically for people's sanity)
        ListIterator<ConfigurableEnchantment> iter = EnchantmentManager.CONFIGURABLE_ENCHANTS.stream().sorted((e1, e2) -> {
            String s1 = I18n.translate("enchantment." + ExtendedEnchants.MOD_ID + "." + e1.getConfigMediator().getKey());
            String s2 = I18n.translate("enchantment." + ExtendedEnchants.MOD_ID + "." + e2.getConfigMediator().getKey());
            return s1.compareTo(s2);
        }).toList().listIterator();
        while (iter.hasNext()) {
            int verticalOffset = iter.nextIndex() * VanillaSettingsEntryMimicWidget.DEFAULT_HEIGHT;
            ConfigurableEnchantment enchantment = iter.next();
            WWidget navWidget = generateNavElement(enchantment);
            contentsPanel.addCentered(navWidget, verticalOffset);
        }

        return contentsPanel;
    }

    /**
     * Generate a button which resets all configs in this mod
     */
    protected static WButton generateResetAllButton() {
        WButton resetButton = new WButton();
        resetButton.setLabel(ExtendedEnchants.buildLangKey("widget", "label", "reset_all"));
        resetButton.setOnClick(ConfigManager::resetAllConfigs);
        return resetButton;
    }

    /**
     * Generate a navigation element that allows navigation to a given enchantment's configuration,
     * as well as a reset for the enchantment's configuration values
     * @param enchantment The configurable enchantment that should be represented
     */
    protected static WWidget generateNavElement(ConfigurableEnchantment enchantment) {
        // The language key that should be used to label this entry
        MutableText text = ExtendedEnchants.buildLangKey(
                "enchantment", enchantment.getConfigMediator().getKey()
        ).setStyle(Style.EMPTY.withColor(0xFFFFFF));

        // A button which redirects the player to the enchantment's configuration options
        WButton configButton = new WButton(ExtendedEnchants.buildLangKey("widget", "label", "configure"));
        configButton.setOnClick(() -> {
            MinecraftClient client = MinecraftClient.getInstance();
            client.setScreen(EnchantmentConfigScreen.build(enchantment, client.currentScreen));
        });

        // A button which resets the enchantment's settings back to default
        WButton resetButton = new WButton(ExtendedEnchants.buildLangKey("widget", "label", "reset"));
        resetButton.setOnClick(() -> enchantment.getConfigMediator().resetAll());

        return new VanillaSettingsEntryMimicWidget(text, configButton, resetButton, 250);
    }
}
