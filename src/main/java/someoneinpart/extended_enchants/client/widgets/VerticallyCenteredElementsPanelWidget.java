package someoneinpart.extended_enchants.client.widgets;

import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WWidget;

import java.util.ArrayList;

/**
 * A Plain Panel which will automatically centers the elements  it's been
 * specified to along the vertical axis as it changes size. Primarily
 * intended to be used on FullScreen GUIs.
 */
public class VerticallyCenteredElementsPanelWidget extends WPlainPanel {

    ArrayList<WWidget> centeredWidgets = new ArrayList<>();

    public void addCentered(WWidget w, int y, int width, int height) {
        // Link everything together as needed
        children.add(w);
        centeredWidgets.add(w);
        w.setParent(this);

        // Update the size of the element to match the specified size, if possible
        if (w.canResize()) {
            w.setSize(width, height);
        }

        // Move the widget to the center of this panel
        int centerPos = calculateCenterPos(w);
        w.setLocation(centerPos, y);

        // Grow this panel if the widget doesn't fit
        expandToFit(w, insets);
    }

    public void addCentered(WWidget w, int y) {
        this.addCentered(w, y, 18, 18);
    }

    @Override
    public void setSize(int x, int y) {
        // Update the size of the panel as usual
        super.setSize(x, y);
        // Move all widgets back to the center of the panel
        for (WWidget w : centeredWidgets) {
            int centerPos = calculateCenterPos(w);
            w.setLocation(centerPos, w.getY());
        }
    }

    protected int calculateCenterPos(WWidget widget) {
        // Calculate the centered positions of the widget
        int widgetWidth = widget.getWidth();
        int panelWidth = this.width;
        return Math.max((panelWidth / 2) - (widgetWidth / 2), 0);
    }
}
