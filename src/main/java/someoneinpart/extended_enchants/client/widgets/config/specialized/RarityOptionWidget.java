package someoneinpart.extended_enchants.client.widgets.config.specialized;

import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import net.minecraft.util.Formatting;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.widgets.config.generalized.EnumOptionWidget;
import someoneinpart.extended_enchants.config.options.generalized.EnumOption;

import java.util.EnumMap;

import static net.minecraft.enchantment.Enchantment.Rarity;

/**
 * An extension to provide nicer handling of the rarity property of most enchants,
 * matching the rarity colors used by certain items in the base game
 */
public class RarityOptionWidget extends EnumOptionWidget<Rarity> {

    // A text map for easy text querying as needed
    protected static EnumMap<Rarity, MutableText> textMap = new EnumMap<>(Rarity.class) {{
        put(Rarity.COMMON, ExtendedEnchants.buildLangKey("enum", Rarity.COMMON.toString().toLowerCase())
                .setStyle(Style.EMPTY.withColor(Formatting.WHITE)));
        put(Rarity.UNCOMMON, ExtendedEnchants.buildLangKey("enum", Rarity.UNCOMMON.toString().toLowerCase())
                .setStyle(Style.EMPTY.withColor(Formatting.YELLOW)));
        put(Rarity.RARE, ExtendedEnchants.buildLangKey("enum", Rarity.RARE.toString().toLowerCase())
                .setStyle(Style.EMPTY.withColor(Formatting.AQUA)));
        put(Rarity.VERY_RARE, ExtendedEnchants.buildLangKey("enum", Rarity.VERY_RARE.toString().toLowerCase())
                .setStyle(Style.EMPTY.withColor(Formatting.LIGHT_PURPLE)));
    }};

    public RarityOptionWidget(EnumOption<Rarity> option) {
        super(option);
    }

    @Override
    public void onChange() {
        Rarity newVal = (Rarity) boundOption.getValue();
        this.setLabel(textMap.get(newVal));
    }
}
