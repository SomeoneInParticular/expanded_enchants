package someoneinpart.extended_enchants.client.widgets.config.generalized;

import io.github.cottonmc.cotton.gui.widget.WButton;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.options.generalized.EnumOption;

public class EnumOptionWidget<T extends Enum<T>> extends WButton implements ConfigBound {

    // The option bound to this element
    protected EnumOption<T> boundOption;

    public EnumOptionWidget(EnumOption<T> option) {
        super();
        this.boundOption = option;
        this.setOnClick(this::cycleValue);
        this.onChange();
    }

    @Override
    public void onChange() {
        String newKey = boundOption.getValue().toString().toLowerCase();
        this.setLabel(ExtendedEnchants.buildLangKey("enum", newKey));
    }

    protected void cycleValue() {
        Class<T> enumClass = boundOption.getEnumClass();
        T[] constants = enumClass.getEnumConstants();
        int newOrdinal = (boundOption.getValue().ordinal() + 1) % constants.length;
        T newValue = constants[newOrdinal];
        boundOption.setValue(newValue);
        this.onChange();
    }
}
