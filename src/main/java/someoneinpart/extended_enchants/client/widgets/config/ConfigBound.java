package someoneinpart.extended_enchants.client.widgets.config;

/**
 * Typing interface that represents the capacity for this widget to be bound to
 * a config option, updating in response to changes in said config option.
 * Should only be used on WWidget subclasses!
 */
public interface ConfigBound {
    /**
     * When called, this function which should update the widget to re-evaluate
     * its state (usually when the underlying config option has changed as the results
     * of something other than the widget itself)
     */
    void onChange();
}
