package someoneinpart.extended_enchants.client.widgets.config.generalized;

import io.github.cottonmc.cotton.gui.widget.WButton;
import net.minecraft.text.MutableText;
import net.minecraft.text.Style;
import someoneinpart.extended_enchants.ExtendedEnchants;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.options.generalized.BooleanOption;

public class BooleanOptionWidget extends WButton implements ConfigBound {

    // Enabled Text
    public static MutableText ENABLED_TEXT = ExtendedEnchants.buildLangKey("widget", "label", "enabled")
            .setStyle(Style.EMPTY.withColor(0x22aa22));
    // Disabled Text
    public static MutableText DISABLED_TEXT = ExtendedEnchants.buildLangKey("widget", "label", "disabled")
            .setStyle(Style.EMPTY.withColor(0xaa2222));

    // The option bound to this element
    protected BooleanOption boundOption;

    public BooleanOptionWidget(BooleanOption option) {
        super();
        this.boundOption = option;
        this.setOnClick(this::toggleState);
        this.onChange();
    }

    @Override
    public void onChange() {
        if (boundOption.getValue()) {
            this.setLabel(ENABLED_TEXT);
        } else {
            this.setLabel(DISABLED_TEXT);
        }
    }

    // Switches the state of the button (from true to false, and vice versa)
    protected void toggleState() {
        // Update the bound options value
        boolean newVal = !boundOption.getValue();
        boundOption.setValue(newVal);

        // Update the widget to match
        this.onChange();
    }
}
