package someoneinpart.extended_enchants.client.widgets.config.generalized;

import io.github.cottonmc.cotton.gui.widget.WLabeledSlider;
import net.minecraft.text.LiteralText;
import someoneinpart.extended_enchants.client.widgets.config.ConfigBound;
import someoneinpart.extended_enchants.config.options.generalized.IntegerOption;

public class IntegerOptionWidget extends WLabeledSlider implements ConfigBound {

    // The option bound to this element
    protected IntegerOption boundOption;

    public IntegerOptionWidget(IntegerOption option, int min, int max) {
        super(min, max);
        this.boundOption = option;
        this.setLabelUpdater(this::updateLabel);
        this.onChange();
    }

    @Override
    public void onChange() {
        int newVal = boundOption.getValue();
        this.value = newVal;
        this.onValueChanged(newVal);
    }

    @Override
    protected void onValueChanged(int value) {
        super.onValueChanged(value);
        boundOption.setValue(value);
    }

    protected LiteralText updateLabel(int value) {
        return new LiteralText(Integer.toString(value));
    }
}
