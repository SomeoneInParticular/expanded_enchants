package someoneinpart.extended_enchants.client.widgets;

import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.WWidget;
import io.github.cottonmc.cotton.gui.widget.data.VerticalAlignment;
import net.minecraft.text.MutableText;

/**
 * A widget aimed to mimic the appearance and functionality of a row within the
 * vanilla settings (specifically the KeyBind menu), but with LibGUIs dark mode
 * support and the ability to accept any pair of widgets.
 *
 * Built primarily to be used alongside
 * {@link someoneinpart.extended_enchants.client.gui.VanillaSettingsMimicGUI},
 * though it can be used elsewhere as well
 */
public class VanillaSettingsEntryMimicWidget extends WPlainPanel {
    // The default width the first element should occupy
    public static final int DEFAULT_FIRST_WIDTH = 75;
    // The default width our second element should occupy
    public static final int DEFAULT_SECOND_WIDTH = 50;

    // The default x position offsets (from the right of the screen) for the first widget
    public static final int DEFAULT_FIRST_OFFSET = 135;
    // The default x position offsets (from the right of the screen) for the second widget
    public static final int DEFAULT_SECOND_OFFSET = 50;

    // The default width this entry should occupy
    public static final int DEFAULT_WIDTH = 300;
    // The default height this entry should occupy
    public static final int DEFAULT_HEIGHT = 20;


    public VanillaSettingsEntryMimicWidget(MutableText text, WWidget widget1, WWidget widget2, int width) {
        // Generate and position the label for this entry
        WLabel label = new WLabel(text);
        label.setVerticalAlignment(VerticalAlignment.CENTER);
        label.disableDarkmode();
        this.add(label, 0, 0);

        // Position the first widget
        int pos1 = width - DEFAULT_FIRST_OFFSET;
        this.add(widget1, pos1, 0, DEFAULT_FIRST_WIDTH, DEFAULT_HEIGHT);

        // Position the second widget
        int pos2 = width - DEFAULT_SECOND_OFFSET;
        this.add(widget2, pos2, 0, DEFAULT_SECOND_WIDTH, DEFAULT_HEIGHT);
    }

    public VanillaSettingsEntryMimicWidget(MutableText text, WWidget widget1, WWidget widget2) {
        this(text, widget1, widget2, DEFAULT_WIDTH);
    }
}
