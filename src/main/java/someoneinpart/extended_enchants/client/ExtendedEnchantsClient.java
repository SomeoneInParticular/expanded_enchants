package someoneinpart.extended_enchants.client;

import net.fabricmc.api.ClientModInitializer;
import someoneinpart.extended_enchants.ExtendedEnchants;

@net.fabricmc.api.Environment(net.fabricmc.api.EnvType.CLIENT)
public final class ExtendedEnchantsClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        // Do nothing
    }
}
